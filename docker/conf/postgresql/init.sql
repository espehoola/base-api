DO $$
BEGIN
  CREATE ROLE pr_user WITH LOGIN PASSWORD 'superpassword';
  EXCEPTION WHEN OTHERS THEN
  RAISE NOTICE 'Role is already exists';
END
$$;
CREATE DATABASE pr_db OWNER pr_user;

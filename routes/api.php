<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1'], function () {
    Route::post('user', [UserController::class, 'create']);
    Route::post('recovery-request', [PasswordController::class, 'recoveryRequest'])->name('password.recovery');
    Route::post('reset-password', [PasswordController::class, 'resetPassword'])->name('password.reset');

    Route::middleware('auth:api')->group(function () {
        Route::group(['prefix' => 'auth'], function () {
            Route::post('login', [AuthController::class, 'login'])
                ->withoutMiddleware('auth:api')
                //->middleware('throttle:3:1')
                ->name('login');
            Route::post('logout', [AuthController::class, 'logout'])->name('logout');
            Route::post('refresh', [AuthController::class, 'refresh'])                ->withoutMiddleware('auth:api')
                ->name('refresh');
        });

        Route::get('user', [UserController::class, 'show']);
    });
});

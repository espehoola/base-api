#!/bin/bash
docker-compose exec php composer install
docker-compose exec php php artisan migrate
docker-compose exec php php artisan key:generate
docker-compose exec php chmod -R 777 ./storage/framework/cache/data
docker-compose exec php chmod 777 ./storage/logs/laravel.log

<?php

namespace Tests\Fixture;

use App\Models\User;

/**
 * Трэйт фикстуры пользователя
 */
trait UserFixture
{
    /** @var User $user */
    public User $user;

    /**
     * Данные пользователя
     *
     * @var array|string[]
     */
    public array $userData = [
        'first_name' => 'Name',
        'password' => 'qwerty12',
        'email' => 'example@test.com',
    ];

    /**
     * Создаем фикстуру пользователя
     *
     * @return User
     */
    public function createUser(): User
    {
        return $this->user = User::factory()->create();
    }
}

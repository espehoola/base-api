<?php

namespace Tests\Feature;

use Tests\Fixture\UserFixture;
use Tests\TestCase;

/**
 * Тесты, связанные с моделью пользователя
 */
class UserTest extends TestCase
{
    use UserFixture;

    public string $route = 'api/v1/user';

    /**
     * Создание пользователя
     *
     * @return void
     */
    public function testCreateUser(): void
    {
        $response = $this->post($this->route, $this->userData);
        unset($this->userData['password']);
        $response->assertStatus(200);
        $response->assertJsonFragment($this->userData);
    }

    /**
     * Наличие ошибок валидации
     *
     * @return void
     */
    public function testValidationUserCreate(): void
    {
        $response = $this->post($this->route);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors([
            'first_name' => 'The first name field is required.',
            'email' => 'The email field is required.',
            'password' => 'The password field is required.',
        ]);
    }
}

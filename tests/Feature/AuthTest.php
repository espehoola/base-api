<?php

namespace Tests\Feature;

use Tests\Fixture\UserFixture;
use Tests\TestCase;

/**
 * Тесты авторизации
 */
class AuthTest extends TestCase
{
    use UserFixture;

    public string $route = 'api/v1/auth/';

    /**
     * Проверка ошибочной авторизации
     *
     * @return void
     */
    public function testAuthUnauthenticated(): void
    {
        $response = $this->post($this->route . 'login', $this->userData);
        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    /**
     * Проверка авторизации
     *
     * @return void
     */
    public function testAuthSuccess(): void
    {
        $user = $this->createUser();

        $response = $this->post($this->route . 'login',
            [
                'email' => $user->email,
                'password' => 'password',
            ]
        );

        $response->assertStatus(200);
        $response->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
    }
}

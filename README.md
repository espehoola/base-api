###INSTALL
```
docker-compose up -d --build
./init.sh
```
###DEBUG
IDE_KEY = PHPSTORM
Port = 9901
Инструкция: https://thecodingmachine.io/configuring-xdebug-phpstorm-docker

###TESTS
```
./vendor/bin/phpunit
```

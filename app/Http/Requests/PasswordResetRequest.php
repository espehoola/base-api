<?php
declare(strict_types=1);

namespace App\Http\Requests;

/**
 * Валидация токена восстановления пароля
 */
class PasswordResetRequest extends NotAuthorizedRequest
{
    /**
     * Правила валидации
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'token' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ];
    }
}

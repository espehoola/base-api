<?php
declare(strict_types=1);

namespace App\Http\Requests;

/**
 * Валидация запроса восстановления пароля
 */
class PasswordRecoveryRequest extends NotAuthorizedRequest
{
    /**
     * Правила валидации
     *
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
        ];
    }
}

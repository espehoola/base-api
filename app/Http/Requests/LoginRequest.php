<?php

namespace App\Http\Requests;

/**
 * Проверка запроса при логине пользователя
 */
class LoginRequest extends NotAuthorizedRequest
{
    /**
     * Правила валидации
     *
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
        ];
    }
}

<?php

namespace App\Http\Requests;

/**
 * Проверка при обновлении пользователя
 */
class UserUpdateRequest extends NotAuthorizedRequest
{
    /**
     * Правила валидации
     *
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'second_name' => 'string',
            'middle_name' => 'string',
            'phone' => 'number',
        ];
    }

    public function wantsJson(): bool
    {
        return true;
    }
}

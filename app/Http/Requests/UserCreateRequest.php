<?php

namespace App\Http\Requests;

/**
 * Проверка при создании пользователя
 */
class UserCreateRequest extends NotAuthorizedRequest
{
    /**
     * Правила валидации
     *
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'second_name' => 'string',
            'middle_name' => 'string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8',
            'phone' => 'number',
        ];
    }
}

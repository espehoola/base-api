<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Базовый контроллер
 *
 * @OA\Info(
 *    version="1.0",
 *    title="Base API",
 *    description="Swagger OpenApi description",
 * ),
 * @OA\Server(url="http://localhost/api/v1/"),
 * @OA\Schemes(format="http")
 * @OA\Components(
 *     @OA\SecurityScheme(
 *         securityScheme="bearerAuth",
 *         type="http",
 *         name="bearerAuth",
 *         scheme="bearer",
 *     )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}

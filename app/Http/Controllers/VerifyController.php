<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

/**
 * Контроллер восстановление пароля
 */
class VerifyController extends Controller
{
    public ?User $user;

    /**
     * Отправка письма восстановления пароля
     * @param Request $request
     * @return string[]
     * @throws \Exception
     */
    public function email(Request $request)
    {
        $this->user = User::find($request->route('id'));

        if (!hash_equals(
            (string)$request->route('id'),
            (string)$this->user->getKey())
        ) {
            throw new \Exception('Wrong hash');
        }

        if (!hash_equals(
            (string)$request->route('hash'),
            sha1($this->user->getEmailForVerification()))
        ) {
            throw new \Exception('Wrong hash');
        }

        $this->user->markEmailAsVerified();

        event(new Verified($this->user));

        return ['message' => 'verified'];
    }
}

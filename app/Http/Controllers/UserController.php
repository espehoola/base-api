<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Interfaces\Api\UserInterface;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * User контроллер
 */
class UserController extends Controller implements UserInterface
{
    /**
     * Создание пользователя
     *
     * @param UserCreateRequest $request
     * @return array
     */
    public function create(UserCreateRequest $request): array
    {
        $model = new User(array_merge(
            $request->toArray(),
            [
                'password' => Hash::make($request->get('password')),
                'status' => User::STATUS_WAIT,
            ],
        ));
        $model->save();
        event(new Registered($model));

        return $model->toArray();
    }

    /**
     * Отобразить данные пользователя
     *
     * @return array
     */
    public function show(): array
    {
        return ['user' => auth('api')->user()];
    }

    /**
     * Обновить данные пользователя
     *
     * @param UserUpdateRequest $request
     * @param User $user
     * @return array
     */
    public function update(UserUpdateRequest $request, User $user): array
    {
        $user->update($request->all());

        return $user->toArray();
    }

    /**
     * Удалить данного пользователя
     *
     * @param User $user
     * @return array
     */
    public function destroy(User $user): array
    {
        if (auth('api')->user()) {

            $user->update(['status' => User::STATUS_DELETE]);
            auth()->logout();

            return ['response' => 'User has been deleted'];
        }

        throw new NotFoundHttpException();
    }
}

<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\PasswordRecoveryRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Контроллер для восстановления пароля
 */
class PasswordController extends Controller
{
    /**
     * Запрос для восстановения пароля
     *
     * @param PasswordRecoveryRequest $request
     * @return array
     */
    public function recoveryRequest(PasswordRecoveryRequest $request): array
    {
        if (!User::whereEmail($request->get('email'))->exists()) {
            throw new NotFoundHttpException();
        }

        $status = Password::sendResetLink($request->only('email'));

        return ['status' => $status, 'info' => ['email' => $request->email]];
    }

    /**
     * Сброс пароля
     *
     * @param PasswordResetRequest $request
     * @return array
     */
    public function resetPassword(PasswordResetRequest $request): array
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill(['password' => Hash::make($password)])->save();
                $user->setRememberToken(Str::random(60));

                event(new PasswordReset($user));
            }
        );

        return ['status' => $status, 'info' => ['email' => $request->get('email')]];
    }
}

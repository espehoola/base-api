<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Interfaces\Api\AuthInterface;
use Illuminate\Auth\AuthenticationException;

/**
 * Контроллер авторизации
 */
class AuthController extends Controller implements AuthInterface
{
    /**
     * Get a JWT via given credentials.
     *
     * @param LoginRequest $request
     * @return array
     * @throws AuthenticationException
     */
    public function login(LoginRequest $request): array
    {
        $credentials = request(['email', 'password']);

        // @TODO не авторизовывать удаленных пользователей
        if (!$token = auth('api')->attempt($credentials)) {
            throw new AuthenticationException();
        }

        return $this->respondWithToken($token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return array
     */
    public function logout(): array
    {
        auth('api')->logout();

        return ['message' => 'Successfully logged out'];
    }

    /**
     * Refresh a token.
     *
     * @return array
     */
    public function refresh(): array
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     * @return array
     */
    protected function respondWithToken(string $token): array
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => time() + auth('api')->factory()->getTTL() * 60
        ];
    }
}

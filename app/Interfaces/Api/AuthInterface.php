<?php
declare(strict_types=1);

namespace App\Interfaces\Api;

use App\Http\Requests\LoginRequest;

/**
 * Интерфейс аутентификации
 */
interface AuthInterface
{
    /**
     * @OA\Post(
     *     path="/auth/login",
     *     operationId="login",
     *     tags={"Auth"},
     *     summary="Login",
     *     description="Getting access token",
     *     @OA\RequestBody(
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="email",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="password",
     *                 type="string",
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="access_token",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="type",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="expires_in",
     *                  type="integer",
     *              )
     *         )
     *     )
     * )
     */
    public function login(LoginRequest $request): array;

    /**
     * @OA\Post(
     *     path="/auth/logout",
     *     operationId="logout",
     *     tags={"Auth"},
     *     summary="Logged out",
     *     description="Getting out",
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *     )
     * )
     */
    public function logout(): array;

    /**
     * @OA\Post(
     *     path="/auth/refresh",
     *     operationId="refresh",
     *     tags={"Auth"},
     *     summary="Refresh",
     *     description="Refresh access token",
     *     security={{ "bearerAuth":{} }},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *              @OA\Property(
     *                  property="access_token",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="type",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="expires_in",
     *                  type="integer",
     *              )
     *         )
     *     )
     * )
     */
    public function refresh(): array;
}

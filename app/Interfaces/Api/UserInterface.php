<?php
declare(strict_types=1);

namespace App\Interfaces\Api;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;

/**
 * Интерфейс действий над пользователями
 */
interface UserInterface
{
    /**
     * Создание пользователя
     *
     * @param UserCreateRequest $request
     * @return array
     */
    public function create(UserCreateRequest $request): array;

    /**
     * Отобразить данные пользователя
     *
     * @return array
     */
    public function show(): array;

    /**
     * Обновить данные пользователя
     *
     * @param UserUpdateRequest $request
     * @param User $user
     * @return array
     */
    public function update(UserUpdateRequest $request, User $user): array;

    /**
     * Удалить данного пользователя
     *
     * @param User $user
     * @return array
     */
    public function destroy(User $user): array;
}

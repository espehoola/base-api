<?php

namespace App\Models;

use Carbon\CarbonInterface;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\Models\User
 *
 *
/**
 * Модель шаблонов
 *
 * @OA\Schema(
 *     title="User",
 *     type="object",
 *     @OA\Property(
 *         property="first_name",
 *         format="string",
 *         example="Admin"
 *     ),
 *     @OA\Property(
 *         property="email",
 *         format="string",
 *         example="exampla@local.com"
 *     ),
 *     @OA\Property(
 *         property="phone",
 *         format="string",
 *         example="123456789"
 *     ),
 * )
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $second_name
 * @property string|null $middle_name
 * @property string $role admin|user
 * @property string $email
 * @property int|null $phone
 * @property string|null $email_verified_at
 * @property string|null $phone_verified_at
 * @property string $password
 * @property string $remember_token
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAccessToken($value)
 * @method static Builder|User whereBonusBalance($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereExpiredAt($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereMainBalance($value)
 * @method static Builder|User whereMiddleName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRecoveryToken($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User wherePhoneVerifiedAt($value)
 * @method static Builder|User whereSecondName($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use HasFactory,
        Notifiable;

    public const STATUS_WAIT = 2;
    public const STATUS_ACTIVE = 1;
    public const STATUS_DELETE = 0;

    public $timestamps = ['created_at', 'updated_at', 'expired_at'];

    protected $dateFormat = 'U';

    protected $fillable = [
        'first_name',
        'second_name',
        'middle_name',
        'phone',
        'email',
        'password',
        'recovery_token',
    ];

    protected $attributes = [
        'status' => self::STATUS_WAIT,
    ];

    protected $hidden = ['password', 'recovery_token'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }
}
